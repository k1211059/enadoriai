# coding: utf-8
# heis.rb

=begin
#####################################

~~~~~~~~~~~~~~ info ~~~~~~~~~~~~~~~~~
  myBoard         => [:field][i(0..COL+2)][j(0..RAW+2)]
                                   = ID (0...KOMA:own, KOMA...KOMA+KOMA:opp)
                                     -1 (empty)
                                     -2 (walked (for search))
                                     -9 (wall)
                     [:komaNum]   = [number(own), number(opp)]
                     [:turnP]     = 1

  koma[KOMA+KOMA] => [:team]      = 0:own, 1:opp
                     [:alive]     = 0:dead, 1:alive
                     [:health]    <= 2
                     [:attack]    = 1
                     [:walkDis]   = 3
                     [:movable]   = 0:can't, 1:can
                     [:stayable]  = 1
                     [:place]     = [x, y]

  move            => [:turn_team] = "THE DETONATOR"
                     [:contents]  = [{ 
                                        :unit_id => "TH**",
                                        :to => {:x => **, :y => **},
                                        :atk => {:x => **, :y => **} (optional)
                                      },{},...,{}
                                     ]

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~ function ~~~~~~~~~~~~~
  myBoard       = iniBoard()                         : makes empty board.

  void            printBoard(myBoard,koma,mode=0)    : shows board (mode: optional).

  koma[i]       = iniKoma()                          : prepares making koma[i].

  koma[kId]     = receiveKoma(u,mode)                : receive koma[kId]'s info 
                                                       from server.

  move          = eNaDoRiAI(team_name,myBoard,koma)  : makes hash for sending JSON.

  m             = addMove(m,id  ,to     ,atk    )    : pushes one move.
                          int  int[2]  int[2]
                               [x, y]  [x, y]
                                       (optional)

  enable[i]     = searchE(i,myBoard,koma)            : makes movable list.

  atkTo         = attackFrom([x, y],b,mode)          : makes attackable list 
                                                       from (x,y).
                                                       mode=0 : by own
                                                       mode=1 : by opp

  moveTo, atkTo = moveOne([x, y],num,moveTo,atkTo,b,dis) : walk one step 
                                                           in simulative board.
                                                           But there is 
                                                           recursive call in it.

  TRUE or FALSE = walked?(moveTo,p)                  : judges p have been searched.
                                                       But now, it must be false.

  myBoard, koma = updateData(myBoard,koma,i,r,enable) : updates infomation
                                                        of myBoard and koma
                                                        by enable[r+1].

  flag_k, enable = weakerKiller(enable,myBoard,koma) : arrenges enablelist
                                                       for attacking or
                                                       killing enemy.

  enable        = avoidOpp(enable,myBoard,koma)      : dodges opponent's attack.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#####################################
=end

require 'pp'
require 'socket'
require 'json'

BOARDCOL = 20
BOARDRAW = 20
KOMA = 36

MNUM = 100

Thread.abort_on_exception = true

def iniHeis()
  if ARGV[0] == nil then
    puts "usage: hoge.rb (0 or ip) [1 or 2 (th player)] [0 or 1 (for debug)]"
    exit(999)
  elsif ARGV[0] == "0" then
    sock = TCPSocket.open("localhost", 20000)
  else
    sock = TCPSocket.open("192.168.12."+ARGV[0], 20000)
  end
  
  if ARGV[1] == nil || ARGV[1] == "1" then
    team_name = 'THE DETONATOR'
    tn = "TH"
  elsif ARGV[1] == "2" then
    team_name = 'SHE DETONATOR'
    tn = "SH"
  else
    puts "usage: hoge.rb (0 or ip) [1 or 2 (th player)] [0 or 1 (for debug)]"
    exit(999)
  end
  
  if ARGV[2] == "0" then
    showinfo = 0
  elsif ARGV[2] == "1" then
    showinfo = 1
  else
    puts "usage: hoge.rb (0 or ip) [1 or 2 (th player)] [0 or 1 (for debug)]"
    exit(999)
  end
  return sock, team_name, tn, showinfo
end

# サーバ接続 OPEN
sock, team_name, TN, ShowINFO = iniHeis()

#チーム名要求
puts sock.gets

# チーム名の通知
sock.puts JSON.generate({:team_name => team_name})
puts team_name
sock.flush

# ハンドシェイク
puts "ハンドシェイク", sock.gets

def iniBoard
  
  b={}
  
  b.store(:field,[])
  b[:field] = Array.new(BOARDRAW+2){Array.new(BOARDCOL+2)}
  for i in 0...(BOARDRAW+2) do
    for j in 0...(BOARDCOL+2) do
      if i==0 || i==BOARDRAW+2-1 || j==0 || j==BOARDCOL+2-1 then
        b[:field][i][j] = -9
      else
        b[:field][i][j] = -1
      end
    end
  end

  b.store(:komaNum,[0,0]) # own,opp
  
  b.store(:turnP,1)
  
  
  return b
end

def printBoard (bC,kC,modeC=0)
  b = Marshal.load(Marshal.dump(bC))
  k = Marshal.load(Marshal.dump(kC))
  mode = Marshal.load(Marshal.dump(modeC))
  
  counter = 0
  b[:field].flatten.each { |f|
    if f==-1 then 
      print "       "
    elsif f==-9 then
      print "======="
    else
      if mode == 0 then
        print " " + sprintf("%02d",f.to_s) + "_" + k[f][:health].to_s + "_" + (k[f][:movable] == 1 ? "A" : ".")
      elsif mode == 1 then
        if f>99 then
          print " " + sprintf("%6d",(f-100).to_s)
        elsif f >= 0 && f < KOMA then
          print "[     ]"
        elsif f >= KOMA && f < KOMA+KOMA then
          print "[=====]"
        else
          print "       "
        end
      end
    end
    
    counter += 1
    if counter % (BOARDRAW + 2) == 0 then
      puts ""
    end
  }
end

def iniKoma
  k={}

  k.store(:team,0) # 0:own 1:opp
  k.store(:alive,0)
  k.store(:health,0)
  k.store(:attack,1)
  k.store(:walkDis,3)
  k.store(:movable,0)
  k.store(:stayable,1)
  k.store(:place,[0,0])
  
  return k
end

def receiveKoma(uC,mode)
  u = Marshal.load(Marshal.dump(uC))
  
  k = {}

  if mode == 1 then
    k.store(:alive,1)
    k.store(:health,u[:hp])
    k.store(:attack,1)
    k.store(:walkDis,3)
    k.store(:stayable,1)
    k.store(:place,[u[:locate][:x]+1, u[:locate][:y]+1])
  elsif mode == 0 then
    k.store(:alive,0)
    k.store(:health,0)
    k.store(:attack,1)
    k.store(:walkDis,3)
    k.store(:stayable,1)
    k.store(:place,[-1,-1])
  end
  return k
end

# main for making decision
def eNaDoRiAI (tnC,myBoardC,komaC)
  tn = Marshal.load(Marshal.dump(tnC))
  
  maxM = {}
  maxM.store(:turn_team, tn)
  maxM.store(:contents, [])
  maxValue = -1
    
  # make AI here
  
  # for add a move to list
  # maxM = addMove(m,id,to[2],atk[2])
  #                     int to[0]=x, [1]=y
  #                           int atk[0]=x, [1]=y (optional)
  
  koma = Marshal.load(Marshal.dump(komaC))
  # for avoid
  threatArea = createThreat(koma)

  random = Random.new

  for mNum in 0...MNUM do
    
    myBoard = Marshal.load(Marshal.dump(myBoardC))
    koma = Marshal.load(Marshal.dump(komaC))
    
    m = {}
    m.store(:turn_team, tn)
    m.store(:contents, [])
    
    # own move
    # random order for units
    ownOrd = [*0...KOMA]
    ownOrd.shuffle!
    # own loop begin
    ownOrd.each { |i|
      if koma[i][:alive] == 0 then
        next
      end
      
      # scan movable
      enable = searchE(i,myBoard,koma)
      
      # select move
      
      # if he has attackable, remove the others form enablelist.
      #flag_k, enable = weakerKiller(enable,myBoard,koma)
      
      # if he has no-attackable, remove the moves where under threat.
=begin
      if flag_k == 0 then
        enable = avoidOpp(enable,myBoard,koma,threatArea)
      end
=end      
      
      # random select
      r = random.rand(0...enable[0])
      
      if enable[r+1][:doAtk] == 0 then
        m = addMove(m,i,[enable[r+1][:moveTo][0]-1, enable[r+1][:moveTo][1]-1])
      else
        m = addMove(m,i,[enable[r+1][:moveTo][0]-1, enable[r+1][:moveTo][1]-1],[enable[r+1][:atkTo][0]-1, enable[r+1][:atkTo][1]-1])
      end
      
      # update myBoard, koma
      myBoard, koma = updateData(myBoard,koma,i,r,enable)
      
    } # own loop end
    
    # opp move (for simulation)
    # random order for units
    oppOrd = [*KOMA...(KOMA+KOMA)]
    oppOrd.shuffle!
    # opp loop begin
    oppOrd.each { |i|
      if koma[i][:alive] == 0 then
        next
      end
      
      # scan movable
      enable = searchE(i,myBoard,koma,1)
      
      # select move
      
      # if he has attackable, remove the others form enablelist.
      flag_k, enable = weakerKiller(enable,myBoard,koma,0)
      
      # random select
      r = random.rand(0...enable[0])
      
      # update myBoard, koma
      myBoard, koma = updateData(myBoard,koma,i,r,enable)
       
    } # opp loop end
    
    value = evaluBoard(myBoard,koma)
    print mNum.to_s + " th value: " + value.to_s
    puts ""
    if value > maxValue then
      maxValue = value
      maxM = m
    end

  end

  
  return maxM
end

def addMove(mC,idC,toC,atkC=[-1,-1])
  m = Marshal.load(Marshal.dump(mC))
  id = Marshal.load(Marshal.dump(idC))
  to = Marshal.load(Marshal.dump(toC))
  atk = Marshal.load(Marshal.dump(atkC))
  
  a = {}
  a.store(:unit_id, (TN+sprintf("%02d",id.to_s)))
  a.store(:to, {:x => to[0], :y => to[1]})
  if (atk[0]<0) then
    m[:contents] << a
    return m
  end
  a.store(:atk, {:x => atk[0], :y => atk[1]})

  m[:contents] << a
  return m
end

def searchE(inumC,bC,kC,modeC=0)
  inum = Marshal.load(Marshal.dump(inumC))
  b = Marshal.load(Marshal.dump(bC))
  k = Marshal.load(Marshal.dump(kC))
  mode = Marshal.load(Marshal.dump(modeC))
  
  e = [] # [num,{:moveTo => [x,y],:doAtk => 0or1, :atkTo => [x,y](optional)},...]
  e[0] = 0           # movable num
  atkTo = [0]
  
  # stay
  e[0] += 1
  e.push({:moveTo => k[inum][:place], :doAtk => 0})
  
  atkTo = attackFrom(k[inum][:place],b,mode)
  for i in 0...atkTo[0] do
    e[0] += 1
    e.push({:moveTo => k[inum][:place],:doAtk => 1, :atkTo => atkTo[i+1]})
  end
  
  # move
  moveTo = [0]
  atkTo = [0]
  moveTo, atkTo = moveOne(k[inum][:place],1,moveTo,atkTo,b,k[inum][:walkDis],mode)
  
  for i in 0...moveTo[0] do
    e[0] += 1
    if atkTo[i+1][0]==0 then
      e.push({:moveTo => moveTo[i+1], :doAtk => atkTo[i+1][0] })
    else
      e.push({:moveTo => moveTo[i+1], :doAtk => atkTo[i+1][0], :atkTo => atkTo[i+1][1]})
    end
  end

  # remove duplication
  e.uniq!
  e[0] = e.size - 1
  
  return e
end

def attackFrom(pC,bC,modeC) # => [num, [x,y],...]
  p = Marshal.load(Marshal.dump(pC))
  b = Marshal.load(Marshal.dump(bC))
  mode = Marshal.load(Marshal.dump(modeC))

  if mode==0 then
    # enemy
    lId = 1
  elsif mode==1 then
    # own
    lId = 0
  end
  
  to = []
  to.push(0)
  
  if b[:field][p[0]-1][p[1]] / KOMA == lId then
    to.push([p[0]-1,p[1]])
    to[0] += 1
    
  end
  if b[:field][p[0]+1][p[1]] / KOMA == lId then
    to.push([p[0]+1,p[1]])
    to[0] += 1
    
  end
  if b[:field][p[0]][p[1]-1] / KOMA == lId then
    to.push([p[0],p[1]-1])
    to[0] += 1
    
  end
  if b[:field][p[0]][p[1]+1] / KOMA == lId then
    to.push([p[0],p[1]+1])
    to[0] += 1
    
  end
  
  return to
end


def moveOne(pC,numC,moveToC,atkToC,bC,disC,modeC=0)
  p = Marshal.load(Marshal.dump(pC))
  num = Marshal.load(Marshal.dump(numC))
  moveTo = Marshal.load(Marshal.dump(moveToC))
  atkTo = Marshal.load(Marshal.dump(atkToC))
  b = Marshal.load(Marshal.dump(bC))
  dis = Marshal.load(Marshal.dump(disC))
  mode = Marshal.load(Marshal.dump(modeC))
  
  if num > dis then
    return moveTo, atkTo
  end
  
  b[:field][p[0]][p[1]] = -2 # stamped

  if walked?(moveTo,[p[0]-1,p[1]]) == false then # now: force false
    if b[:field][p[0]-1][p[1]] == -1 then
      # don't atk
      moveTo[0] += 1
      atkTo[0] += 1
      moveTo.push([p[0]-1, p[1]])
      atkTo.push([0, [-1,-1]])
      
      #atk
      elmAtkTo = []
      elmAtkTo = attackFrom([p[0]-1, p[1]], b, mode)
      for i in 0...elmAtkTo[0] do
        moveTo[0] += 1
        atkTo[0] += 1
        moveTo.push([p[0]-1, p[1]])
        atkTo.push([1,elmAtkTo[i+1]])
      end
      
      moveTo, atkTo = moveOne([p[0]-1, p[1]], num+1, moveTo, atkTo, b, dis, mode)
      
    elsif b[:field][p[0]-1][p[1]] >= 0 && b[:field][p[0]-1][p[1]] / KOMA == mode then
      # walk on my koma
      moveTo, atkTo = moveOne([p[0]-1, p[1]], num+1, moveTo, atkTo, b, dis, mode)
      
    end
  end
  
  if walked?(moveTo,[p[0]+1,p[1]]) == false then
    if b[:field][p[0]+1][p[1]] == -1 then
      # don't atk
      moveTo[0] += 1
      atkTo[0] += 1
      moveTo.push([p[0]+1, p[1]])
      atkTo.push([0, [-1,-1]])
      
      #atk
      elmAtkTo = []
      elmAtkTo = attackFrom([p[0]+1, p[1]], b, mode)
      for i in 0...elmAtkTo[0] do
        moveTo[0] += 1
        atkTo[0] += 1
        moveTo.push([p[0]+1, p[1]])
        atkTo.push([1,elmAtkTo[i+1]])
      end
      
      moveTo, atkTo = moveOne([p[0]+1, p[1]], num+1, moveTo, atkTo, b, dis, mode)
      
    elsif b[:field][p[0]+1][p[1]] >= 0 && b[:field][p[0]+1][p[1]] / KOMA == mode then
      # walk on my koma
      moveTo, atkTo = moveOne([p[0]+1, p[1]], num+1, moveTo, atkTo, b, dis, mode)
      
    end
  end

  if walked?(moveTo,[p[0],p[1]-1]) == false then
    if b[:field][p[0]][p[1]-1] == -1 then
      # don't atk
      moveTo[0] += 1
      atkTo[0] += 1
      moveTo.push([p[0], p[1]-1])
      atkTo.push([0, [-1,-1]])
      
      #atk
      elmAtkTo = []
      elmAtkTo = attackFrom([p[0], p[1]-1], b, mode)
      for i in 0...elmAtkTo[0] do
        moveTo[0] += 1
        atkTo[0] += 1
        moveTo.push([p[0], p[1]-1])
        atkTo.push([1,elmAtkTo[i+1]])
      end
      
      moveTo, atkTo = moveOne([p[0], p[1]-1], num+1, moveTo, atkTo, b, dis, mode)
      
    elsif b[:field][p[0]][p[1]-1] >= 0 && b[:field][p[0]][p[1]-1] / KOMA == mode then
      # walk on my koma
      moveTo, atkTo = moveOne([p[0], p[1]-1], num+1, moveTo, atkTo, b, dis, mode)
      
    end
  end
  
  if walked?(moveTo,[p[0],p[1]+1]) == false then
    if b[:field][p[0]][p[1]+1] == -1 then
      # don't atk
      moveTo[0] += 1
      atkTo[0] += 1
      moveTo.push([p[0], p[1]+1])
      atkTo.push([0, [-1,-1]])
      
      #atk
      elmAtkTo = []
      elmAtkTo = attackFrom([p[0], p[1]+1], b, mode)
      for i in 0...elmAtkTo[0] do
        moveTo[0] += 1
        atkTo[0] += 1
        moveTo.push([p[0], p[1]+1])
        atkTo.push([1,elmAtkTo[i+1]])
      end
      
      moveTo, atkTo = moveOne([p[0], p[1]+1], num+1, moveTo, atkTo, b, dis, mode)
      
    elsif b[:field][p[0]][p[1]+1] >= 0 && b[:field][p[0]][p[1]+1] / KOMA == mode then
      # walk on my koma
      moveTo, atkTo = moveOne([p[0], p[1]+1], num+1, moveTo, atkTo, b, dis, mode)
      
    end
  end
  
  return moveTo, atkTo
end

def walked?(moveToC,pC)
=begin
  moveTo = Marshal.load(Marshal.dump(moveToC))
  p = Marshal.load(Marshal.dump(pC))

  for i in 0...moveTo[0] do
    if moveTo[i+1][0] == p[0] && moveTo[i+1][1] == p[1] then
      return true
    end
  end
=end
  return false
end

def updateData(myBoardC,komaC,inumC,rC,enableC)
  myBoard = Marshal.load(Marshal.dump(myBoardC))
  koma = Marshal.load(Marshal.dump(komaC))
  inum = Marshal.load(Marshal.dump(inumC))
  r = Marshal.load(Marshal.dump(rC))
  enable = Marshal.load(Marshal.dump(enableC))
  
  myBoard[:field][koma[inum][:place][0]][koma[inum][:place][1]] = -1
  myBoard[:field][enable[r+1][:moveTo][0]][enable[r+1][:moveTo][1]] = inum
  koma[inum][:movable] = 0
  koma[inum][:place][0] = enable[r+1][:moveTo][0]
  koma[inum][:place][1] = enable[r+1][:moveTo][1]
  
  if enable[r+1][:doAtk] == 1 then
    atkPl = [enable[r+1][:atkTo][0], enable[r+1][:atkTo][1]]
    atkNum = myBoard[:field][atkPl[0]][atkPl[1]]
    koma[atkNum][:health] -= 1
    if koma[atkNum][:health] <= 0 then
      myBoard[:field][atkPl[0]][atkPl[1]] = -1
      myBoard[:komaNum][1] -= 1
      koma[atkNum][:alive] = 0
      koma[atkNum][:place][0] = 0
      koma[atkNum][:place][1] = 0
    end
  end
  
  return myBoard, koma
end

def weakerKiller(enableC,myBoardC,komaC,modeC=0)
  enable = Marshal.load(Marshal.dump(enableC))
  myBoard = Marshal.load(Marshal.dump(myBoardC))
  koma = Marshal.load(Marshal.dump(komaC))

  # have atk in enable?
  flag_l = 0
  for l in 0...enable[0] do
    if enable[l+1][:doAtk] == 1 then
      flag_l = 1
      break
    end
  end

  if flag_l == 0 then return 0, enable end

  # remove non-atk move
  for l in 0...enable[0] do
    if l >= enable[0] then break end
    
    if enable[l+1][:doAtk] == 0 then
      enable.delete_at(l+1)
      enable[0] -= 1
      redo
    end
  end

  if modeC == 1 then return 1,enable end

  # atk to weaker
  opp = [0, 0]
  oppNum = 0
  oppHp = [0]
  weakerHp = 10
  
  for i in 0...enable[0] do
    opp = [enable[i+1][:atkTo][0], enable[i+1][:atkTo][1]]
    oppNum = myBoard[:field][opp[0]][opp[1]]

    oppHp.push(koma[oppNum][:health])
    oppHp[0] += 1
    
    if weakerHp > koma[oppNum][:health] then
      weakerHp = koma[oppNum][:health]
    end
  end
  for i in 0...enable[0] do
    if i >= enable[0] then break end

    if oppHp[i+1] > weakerHp then
      enable.delete_at(i+1)
      enable[0] -= 1
      oppHp.delete_at(i+1)
      oppHp[0] -= 1
      redo
    end
  end

  return 1, enable
end

def avoidOpp(enableC,myBoardC,komaC,tAreaC)
  enable = Marshal.load(Marshal.dump(enableC))
  myBoard = Marshal.load(Marshal.dump(myBoardC))
  koma = Marshal.load(Marshal.dump(komaC))
  tArea = Marshal.load(Marshal.dump(tAreaC))

  eC = enable[1]

  for l in 0...enable[0] do
    if l >= enable[0] then break end

    if tArea.include?(enable[l+1][:moveTo]) then
      enable.delete_at(l+1)
      enable[0] -= 1
      redo
    end
  end

  # surrounded
  if enable[0] <= 0 then
    enable[0] = 1
    enable[1] = eC
  end
  
  return enable
end

def createThreat(komaC)
  k = Marshal.load(Marshal.dump(komaC))
  b = iniBoard()

  for i in KOMA...(KOMA+KOMA) do
    if k[i][:alive] == 0 then next end

    for x in 1...(BOARDCOL+2-1) do
      for y in 1...(BOARDRAW+2-1) do
        dis = (x-k[i][:place][0]).abs + (y-k[i][:place][1]).abs
        if dis <= 4 then
          b[:field][x][y] = -2
        end
      end
    end
  end

  tArea = []

  for x in 1...(BOARDCOL+2-1) do
    for y in 1...(BOARDRAW+2-1) do
      if b[:field][x][y] == -2 then
        tArea.push([x, y])
      end
    end
  end
  
  return tArea
end

def evaluBoard(bC,kC)
  b = Marshal.load(Marshal.dump(bC))
  k = Marshal.load(Marshal.dump(kC))

  aliveMAX = 0.8
  freeMAX = 0.2
  
  e = 0

  ownP = 0
  for i in 0...KOMA do
    ownP += (k[i][:health] + 2*k[i][:alive])
  end

  oppP = 0
  for i in KOMA...(KOMA+KOMA) do
    oppP += (k[i][:health] + 2*k[i][:alive])
  end

  rate = 1.0 * ownP / oppP
  e += (rate > 2.0 ? aliveMAX : (aliveMAX/2.0) * rate)

  freeP = 0
  for i in 0...KOMA do
    x = k[i][:place][0]
    y = k[i][:place][1]
    freeP += (b[:field][x-1][y] == -1 || b[:field][x-1][y] >= KOMA ? 1 : 0)
    freeP += (b[:field][x+1][y] == -1 || b[:field][x+1][y] >= KOMA ? 1 : 0)
    freeP += (b[:field][x][y-1] == -1 || b[:field][x][y-1] >= KOMA ? 1 : 0)
    freeP += (b[:field][x][y+1] == -1 || b[:field][x][y+1] >= KOMA ? 1 : 0)
  end

  rate = (1.0 - 1.0 * freeP / (4.0*b[:komaNum][0]) )
  e += (freeMAX * rate)
  
  

  return (e >= 0.0 ? e : 0.0)
end

# main begin

myBoard = {}
myBoard = iniBoard()

koma = Array.new(KOMA+KOMA)
for i in 0...(KOMA+KOMA) do
  koma[i] = iniKoma()
end

# 盤面情報
loop do
  board = JSON.parse(sock.gets, {:symbolize_names => true})

  break if(board[:finished])

  # 自分のターンのとき
  if(board[:turn_team] == team_name)
    puts "my turn"

    myBoard = iniBoard()
    for i in 0..(KOMA+KOMA) do
      koma[i] = iniKoma()
    end

    myBoard[:komaNum][0] = 0
    myBoard[:komaNum][1] = 0
    aliveK = Array.new(KOMA+KOMA){0}

    board[:units].each { |u|
      if u[:team] == team_name then
        myBoard[:komaNum][0] += 1
        kId = u[:unit_id].delete("^0-9").to_i
        koma[kId][:team] = 0
        koma[kId][:movable] = 1
      else
        myBoard[:komaNum][1] += 1
        kId = u[:unit_id].delete("^0-9").to_i + KOMA
        koma[kId][:team] = 1
        koma[kId][:movable] = 0
      end

      aliveK[kId] = 1

      myBoard[:field][u[:locate][:x]+1][u[:locate][:y]+1] = kId
      
      koma[kId].update(receiveKoma(u,1))
      
    }

    for kId in 0...KOMA+KOMA do
      if aliveK[kId] == 1 then
        next
      end
      koma[kId][:team] = (kId < KOMA ? 0 : 1)
      koma[kId][:movable] = 0
      koma[kId].update(receiveKoma(nil,0))
    end

    printBoard(myBoard,koma,0)

    move = {}
    move = eNaDoRiAI(team_name,myBoard,koma)
    
    # 実際には行動JSONを送る
    sock.puts JSON.generate(move)
    
    sock.flush

    # 結果を取得
    #puts "001" 
    sG = sock.gets
    #puts "002"
    #pp sG
    result = JSON.parse(sG,{:symbolize_names => true})
    puts result
    if result[:result].size != 0 then
      sleep(5)
    end
  end
end

# ソケット CLOSE
sock.close
